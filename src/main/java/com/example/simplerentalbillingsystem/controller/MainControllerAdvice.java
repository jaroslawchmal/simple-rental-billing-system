package com.example.simplerentalbillingsystem.controller;

import com.example.simplerentalbillingsystem.exception.AppException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class MainControllerAdvice {

    @ExceptionHandler({AppException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<?> handleClientNotFoundException(AppException e){
        return ResponseEntity.internalServerError().body(e.getMessage());
    }
}
