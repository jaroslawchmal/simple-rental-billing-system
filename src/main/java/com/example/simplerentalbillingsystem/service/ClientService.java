package com.example.simplerentalbillingsystem.service;

import com.example.simplerentalbillingsystem.entity.Client;
import com.example.simplerentalbillingsystem.exception.AppException;
import com.example.simplerentalbillingsystem.repository.ClientRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ClientService {
    private final ClientRepository repository;
    public Client get(Long id){
        if(repository.existsById(id)) return repository.findById(id).get();
        else throw new AppException("Client not found");
    }
}
