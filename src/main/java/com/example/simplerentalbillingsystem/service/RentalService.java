package com.example.simplerentalbillingsystem.service;

import com.example.simplerentalbillingsystem.entity.Client;
import com.example.simplerentalbillingsystem.entity.Product;
import com.example.simplerentalbillingsystem.entity.Rental;
import com.example.simplerentalbillingsystem.exception.AppException;
import com.example.simplerentalbillingsystem.repository.RentalRepository;
import com.example.simplerentalbillingsystem.request.RentalRequest;
import com.example.simplerentalbillingsystem.response.Report;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class RentalService {
    private final RentalRepository repository;
    private final ProductService productService;
    private final ClientService clientService;
    public void add(RentalRequest request){
        Product product = productService.get(request.getProductName());
        double pricePerMinute = product.getPricePerMinute();
        Client client = clientService.get(request.getClientId());
        boolean dateCorrect = request.getReturnTime().isAfter(request.getRentTime());
        if(!dateCorrect) throw new AppException("Return date cannot be earlier than rent date!");
        Duration period = Duration.between(request.getRentTime(), request.getReturnTime());
        long rentalPeriodInMinutes = period.toMinutes();
        double totalPrice = pricePerMinute*rentalPeriodInMinutes;

        Rental rental = Rental.builder()
                .client(client)
                .product(product)
                .rentDateTime(request.getRentTime())
                .returnDateTime(request.getReturnTime())
                .rentMinutes(rentalPeriodInMinutes)
                .rentPrice(totalPrice)
                .build();

        repository.save(rental);
    }
    public List<Report> findAllByClientAndMonthNo(Client client, long monthNo){
        if(monthNo<1 || monthNo>12) throw new AppException("Incorrect month number!");
        List<Rental> rentalsForClient = repository.findAllByClient(client);
        List<Report> result = new ArrayList<>();
        for (Rental rental: rentalsForClient) {
            int month = rental.getReturnDateTime().getMonthValue();
            if(month==monthNo) {
                Report report = Report.builder()
                    .productName(rental.getProduct().getName())
                    .minutes(rental.getRentMinutes())
                    .price(rental.getRentPrice())
                    .build();
                result.add(report);
            }
        }
        return result;
    }
}
