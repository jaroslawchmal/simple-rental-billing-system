package com.example.simplerentalbillingsystem.repository;

import com.example.simplerentalbillingsystem.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {
}
