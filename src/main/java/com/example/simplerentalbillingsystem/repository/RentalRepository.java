package com.example.simplerentalbillingsystem.repository;

import com.example.simplerentalbillingsystem.entity.Client;
import com.example.simplerentalbillingsystem.entity.Rental;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RentalRepository extends JpaRepository<Rental, Long> {
    List<Rental> findAllByClient(Client client);
}
