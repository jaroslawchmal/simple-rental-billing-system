package com.example.simplerentalbillingsystem.service;

import com.example.simplerentalbillingsystem.entity.Product;
import com.example.simplerentalbillingsystem.exception.AppException;
import com.example.simplerentalbillingsystem.repository.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class ProductService {

    private final ProductRepository repository;

    public List<Product> findAll(){
        if(!repository.findAll().isEmpty()){
            return repository.findAll();
        }
        else throw new AppException("No products found!");
    }

    public void add(Product product){
        repository.save(product);
    }
    public void delete(Long id){
        if(exists(id)) repository.deleteById(id);
        else throw new AppException("Product with id: "+id+" does not exist!");
    }
    public Product get(String name){
        if(exists(name)) return repository.findByName(name);
        else throw new AppException("Product with name: "+name+" does not exist!");
    }
    public boolean exists(String name){
        return repository.existsByName(name);
    }
    public boolean exists(Long id){
        if(id!=null){
            return repository.existsById(id);
        }
        return false;

    }


    public void update(Product product) {
        if(product.getId()!=null){
            repository.save(product);
        }
        else throw new AppException("No id given!");
    }
}
