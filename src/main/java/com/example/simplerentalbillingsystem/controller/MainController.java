package com.example.simplerentalbillingsystem.controller;

import com.example.simplerentalbillingsystem.entity.Client;
import com.example.simplerentalbillingsystem.entity.Product;
import com.example.simplerentalbillingsystem.request.RentalRequest;
import com.example.simplerentalbillingsystem.response.Report;
import com.example.simplerentalbillingsystem.service.ClientService;
import com.example.simplerentalbillingsystem.service.ProductService;
import com.example.simplerentalbillingsystem.service.RentalService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class MainController {

    private final ProductService productService;
    private final RentalService rentalService;
    private final ClientService clientService;

    @PostMapping("/prices")
    public ResponseEntity<?> addProduct(@RequestBody Product product) {
        if(productService.exists(product.getName())){
            return ResponseEntity.badRequest().body("Product with this name already exists! Use another name or make request with PUT method to update");
        }
        else{
            productService.add(product);
            return ResponseEntity.ok("Added product to database");

        }
    }

    @PutMapping("/prices")
    public ResponseEntity<?> editProduct(@RequestBody Product product) {
        if(productService.exists(product.getId())){
            productService.update(product);
            return ResponseEntity.ok().body("Product updated!");
        }
        else{
            return ResponseEntity.badRequest().body("Incorrect product id!");

        }
    }

    @PostMapping("/rentals")
    public ResponseEntity<?> addRental(@RequestBody RentalRequest request) {
        rentalService.add(request);
        return ResponseEntity.ok("Added rental to database");

    }

    @GetMapping("/reports/client/{client_id}/month/{month_no}")
    public ResponseEntity<?> getReport(@PathVariable("client_id") Long clientId, @PathVariable("month_no") Long monthNo) {
        Client client = clientService.get(clientId);
        List<Report> rentals = rentalService.findAllByClientAndMonthNo(client, monthNo);
        return ResponseEntity.ok(rentals);

    }
}
