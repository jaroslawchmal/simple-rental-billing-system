package com.example.simplerentalbillingsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleRentalBillingSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleRentalBillingSystemApplication.class, args);
    }

}
