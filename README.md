# Prosty system rozliczeń dla wypożyczalni urządzeń

## DOMYŚLNA KONFIGURACJA
> BAZA DANYCH MYSQL 
> 
> - nazwa bazy: appdata
> 
> - użytkownik: user
> 
> - hasło: password
> 
> - port: 30306 
> 
>konfigurację można zmienić w pliku application.yml

### ENDPOINTY:
** **
* /api/prices - umożliwia wprowadzanie i edycję wyceny produktów (cennik) - ile kosztuje minuta używania produktu XYZ. 
  * Dodawanie produktu: metoda POST, body: `{ "name": "NAZWA_PRZEDMIOTU", "pricePerMinute": CENA_ZA_MINUTĘ } `
  * Edycja produktu: metoda PUT, body: `{ "id": ID, "name": "NAZWA_PRZEDMIOTU", "pricePerMinute": CENA_ZA_MINUTĘ }`
* /api/rentals - ładowanie czasu użycia danego przedmiotu przez klienta - należy podać id klienta, nazwę przedmiotu, datę wypożyczenia i datę oddania przedmiotu, np.:
  * `{"client_id": 1, "product_name": "PRALKA", "rent_time": "2022-05-07 12:00:00", "return_time": "2022-05-07 13:00:00"}`
  * Czas wypożyczenia oraz wycena są zapisywane do bazy danych
* /api/reports/client/ID_KLIENTA/month/NUMER_MIESIĄCA - raport wypożyczeń za podany miesiąc dla klienta o podanym ID - zwraca nazwę wypożyczonego przedmiotu, czas wypożyczenie w minutach oraz wycenę
