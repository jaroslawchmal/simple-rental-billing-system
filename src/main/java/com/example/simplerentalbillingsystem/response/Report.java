package com.example.simplerentalbillingsystem.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Report {
    private String productName;
    private long minutes;
    private double price;
}
