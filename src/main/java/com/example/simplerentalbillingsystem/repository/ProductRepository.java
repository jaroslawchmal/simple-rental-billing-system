package com.example.simplerentalbillingsystem.repository;

import com.example.simplerentalbillingsystem.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    boolean existsByName(String name);
    Product findByName(String name);
}
