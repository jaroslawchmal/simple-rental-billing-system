-- noinspection SqlNoDataSourceInspectionForFile

SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE rentals;
TRUNCATE TABLE clients;
TRUNCATE TABLE products;
SET FOREIGN_KEY_CHECKS = 1;
INSERT INTO clients (id, name) values (1, "Marek");
INSERT INTO clients (id, name) values (2, "Andrzej");
INSERT INTO products (id, name, price_per_minute) values (1, "PRALKA", 1.5);
INSERT INTO products (id, name, price_per_minute) values (2, "ODKURZACZ", 0.8);
INSERT INTO products (id, name, price_per_minute) values (3, "MYJKA", 1.1);
INSERT INTO rentals (client_id, product_id, rent_time, return_time, rent_minutes, rent_price) values (1, 1, "2022-04-04 12:00:00", "2022-04-04 13:00:00", 60, 90);
INSERT INTO rentals (client_id, product_id, rent_time, return_time, rent_minutes, rent_price) values (1, 2, "2022-05-05 10:20:00", "2022-05-05 10:40:00", 20, 16);
INSERT INTO rentals (client_id, product_id, rent_time, return_time, rent_minutes, rent_price) values (1, 3, "2022-05-06 14:00:00", "2022-05-06 14:15:00", 15, 16.5);
INSERT INTO rentals (client_id, product_id, rent_time, return_time, rent_minutes, rent_price) values (2, 1, "2022-05-04 12:00:00", "2022-05-04 13:00:00", 60, 90);
INSERT INTO rentals (client_id, product_id, rent_time, return_time, rent_minutes, rent_price) values (2, 2, "2022-03-05 10:20:00", "2022-03-05 10:40:00", 20, 16);
INSERT INTO rentals (client_id, product_id, rent_time, return_time, rent_minutes, rent_price) values (2, 3, "2022-05-06 14:00:00", "2022-05-06 14:15:00", 15, 16.5);